# Summary

**aoefileformat** is a Python library for parsing file formats for Age of
Empires 3 and Age of Empires 3: Definitive Edition.

# Install

## From Git

Clone this repository. Install [Python](https://www.python.org/downloads) 3.6+,
adding it to your `PATH` and including pip (both of these are options in the 
installer on Windows). Install by running this command:

```
$ pip install .
```

Run the tools via the command line as specified below. 

# Uninstall

```
$ pip uninstall aoefile
```

# CLI Tools

## Bar Editor

This tool can extrract and build BAR archives.

```
usage: bar-edit [-h] {build,list,extract} ...

Process AoE3 .BAR files.

positional arguments:
  {build,list,extract}
    build               Create a BAR archive
    list                List files in a BAR archive
    extract             Extract files from a BAR archive

optional arguments:
  -h, --help            show this help message and exit
```

### Examples

List files in a BAR archive:
```
$ bar-edit list Data.bar
```

Extract a BAR archive to a directory:
```
$ bar-edit extract Data.bar extracted-data/
```

Extract a single item from a BAR archive:
```
$ bar-edit extract --item protoy.xml.XMB Data.bar .
```

## File Converter

This tool can convert between XML and XMB formats.

```
usage: xmb-conv [-h] [-o OUTPUT] [--format FORMAT] file

Convert between AoE3 file types.

positional arguments:
  file                  Input file

                        Use 'all' to convert all files
                        in directory and subdirs

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output file; defaults to standard out,
                        ignored if 'all' option is used
  --format FORMAT       Output format: xmb|xml|raw_xmb
  --unconvert           Re-converts XMLs to XMBs if 'all' option
                        is used
```

### Examples

For example, to convert XMB to XML:
```
$ xmb-conv -o civs.xml civs.xml.xmb
```

Convert XML back to XMB:
```
$ xmb-conv -o civs.xml.xmb civs.xml
``` 

Convert all XMBs to XMLs, and vice versa:
```
$ xmb-conv all
$ xmb-conv all --unconvert
```