import setuptools

setuptools.setup(
    name='aoefile',
    version='0.1.1',
    description='Library and CLI tools for parsing Age of Empires file formats',
    url='https://gitlab.com/adamjseitz/aoe3-mod-tools',
    packages=['aoefile'],
    keywords=['bar', 'xmb', 'Age of Empires', 'AoE3'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'lxml',
        'lz4',
    ],
    entry_points={
        'console_scripts': [
            'bar-edit=aoefile.barfile:main',
            'xmb-conv=aoefile.xmbfile:main',
        ],
    },
)
