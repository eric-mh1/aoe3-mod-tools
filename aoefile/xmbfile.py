#!/usr/bin/env python3
"""
Convert between AoE3's XMB format and XML files
"""
import argparse
import collections
import enum
import io
import struct
import sys
import zlib
import os

from lxml import etree

from .alz4 import ALZ4_MAGIC, compress_alz4, decompress_alz4
from .exception import AoEFileFormatException

L33T_MAGIC = b'l33t'

class FileType(enum.Enum):
    UNKNOWN = 0
    L33T = 1
    RAW_XMB = 2
    XML = 3
    ALZ4 = 4

class XMBConversionException(AoEFileFormatException):
    """
    XMB conversion exception base
    """
    pass

class L33tInvalidStream(XMBConversionException):
    """
    Raised during L33T stream decompression
    """
    pass

def compress_l33t(data):
    """
    Compress a l33t data stream
    """
    l33t_data = struct.pack('<4sI', L33T_MAGIC, len(data))
    l33t_data += zlib.compress(data)
    return l33t_data

def decompress_l33t(data):
    """
    Decompress a l33t data stream

    l33t is just zlib with a magic and size prepended
    """
    magic, size = struct.unpack_from('<4sI', data)

    if magic != L33T_MAGIC:
        raise L33tInvalidStream('Invalid l33t stream: bad magic "{}"'.format(magic))

    output = zlib.decompress(data[8:])
    if len(output) != size:
        raise L33tInvalidStream('Invalid l33t stream: incorrect size "{}"'.format(size))

    return output

def to_ustr(s):
    if not s:
        s = ''
    data = struct.pack('<I', len(s))
    data += s.encode('utf-16-le')
    return data

XMB_MAGIC = b'X1'
XMB_ROOT_MAGIC = b'XR'
XMB_ROOT_HDR = '<2sII'
XMB_UNK_VAL = 4

XMB_NODE_MAGIC = b'XN'
XMB_NODE_HDR = '<2sI'
XMB_NODE_HDR_SZ = struct.calcsize(XMB_NODE_HDR)
XMB_NODE_META_SZ = struct.calcsize('<IIIII')
XMB_NODE_ATTR_SZ = struct.calcsize('<II')

XMB_ROOT_FMT = '<2sII' # root_hdr, unk, frmt
XMB_ROOT_META_SZ = struct.calcsize(XMB_ROOT_FMT + 'II')

class XMBFormat(enum.IntEnum):
    AOM = 7
    AOE3 = 8

class XMBException(Exception):
    pass

class XMBUnknownFormat(XMBException):
    def __init__(self, format_value):
        self.format_value = format_value
        super().__init__('XMB format unknown: {}'.format(format_value))

class XMBNode:
    def __init__(self, name, text='', lineno=-1, attrs=None, children=None):
        self.name = name
        self.text = text
        self.lineno = lineno
        self.attrs = collections.OrderedDict(attrs if attrs is not None else [])
        self.children = children if children is not None else []

    @classmethod
    def from_xml(cls, xml):
        """Build an XMBNode from an XML string"""
        tree = etree.ElementTree(etree.XML(xml))
        root = tree.getroot()
        return cls.from_xml_element(root)

    @classmethod
    def from_xml_element(cls, xml_element):
        """Build an XMBNode from an XML Element"""
        text = ''
        if xml_element.text:
            text = xml_element.text.strip()

        node = cls(xml_element.tag, text)

        for key, value in xml_element.attrib.items():
            node.attrs[key] = value

        for child_el in xml_element:
            node.children.append(cls.from_xml_element(child_el))

        return node

    def xmb_length(self):
        """
        Get the length of the corresponding XMB data
        """
        return XMB_NODE_HDR_SZ + self._body_length()

    def _body_length(self):
        size = XMB_NODE_META_SZ + XMB_NODE_ATTR_SZ * len(self.attrs)
        if self.text:
            size += len(self.text.encode('utf-16-le'))

        for attr_val in self.attrs.values():
            size += len(attr_val.encode('utf-16-le'))

        for child in self.children:
            size += child.xmb_length()

        return size

    def write_xmb(self, stream, xmb_writer):
        el_id = xmb_writer.elements[self.name]

        stream.write(struct.pack('<2sI',
            XMB_NODE_MAGIC, self._body_length()))

        stream.write(to_ustr(self.text))
        stream.write(struct.pack('<I', el_id))
       
        if xmb_writer.format == XMBFormat.AOE3:
            if self.lineno != -1:
                stream.write(struct.pack('<I', self.lineno))
            else:
                stream.write(struct.pack('<I', 0))

        stream.write(struct.pack('<I', len(self.attrs)))
        for attr_name, attr_val in self.attrs.items():
            attr_id = xmb_writer.attributes[attr_name]
            stream.write(struct.pack('<I', attr_id))
            stream.write(to_ustr(attr_val))

        stream.write(struct.pack('<I', len(self.children)))
        for child in self.children:
            child.write_xmb(stream, xmb_writer)

    def build_xml_node(self):
        """
        Build an lxml.etree.Element for the Node
        """
        root_el = etree.Element(self.name, attrib=self.attrs)
        if self.text:
            root_el.text = self.text
        for child in self.children:
            root_el.append(child.build_xml_node())
        return root_el

    def __str__(self):
        node = self.build_xml_node()
        return etree.tostring(node, encoding='unicode', pretty_print=True)
    
    def __bytes__(self):
        node = self.build_xml_node()
        return etree.tostring(node, pretty_print=True, encoding='utf-8', xml_declaration='True')

class XMBParser:
    def __init__(self):
        self.stream = None
        self.elements = []
        self.attributes = []
        self.format = None
        self.root = None

        self.filetype = FileType.RAW_XMB

    def parse(self, stream):
        self.stream = stream
        self.root = self._parse_root()
        return self.root

    def _read_int(self):
        val, = struct.unpack('<I', self.stream.read(struct.calcsize('<I')))
        return val

    def _read_utf16_str(self):
        str_len = self._read_int()
        return self.stream.read(2 * str_len).decode('utf-16')

    def _parse_node(self):
        hdr, length = struct.unpack(XMB_NODE_HDR, self.stream.read(XMB_NODE_HDR_SZ))

        if hdr != XMB_NODE_MAGIC:
            raise XMBException('Bad node: "{}"'.format(hdr))

        text = self._read_utf16_str()
        name = self.elements[self._read_int()]
        lineno = self._read_int() if self.format == XMBFormat.AOE3 else -1
        num_attrs = self._read_int()

        node = XMBNode(name, text, lineno)

        for _ in range(num_attrs):
            attr_id = self._read_int()
            node.attrs[self.attributes[attr_id]] = self._read_utf16_str()

        num_children = self._read_int()
        for _ in range(num_children):
            node.children.append(self._parse_node())

        return node

    def _parse_root(self):
        hdr_fmt = '<2sI2sII'
        hdr, length, root_hdr, unk, frmt = \
                struct.unpack(hdr_fmt, self.stream.read(struct.calcsize(hdr_fmt)))

        if hdr != XMB_MAGIC:
            raise XMBException('Bad magic: "{}"'.format(hdr))
        if root_hdr != XMB_ROOT_MAGIC:
            raise XMBException('Bad magic: "{}"'.format(root_hdr))
        if unk != XMB_UNK_VAL:
            raise XMBException('Bad header')

        try:
            self.format = XMBFormat(frmt)
        except ValueError:
            raise XMBUnknownFormat(frmt)

        el_count = self._read_int()
        for _ in range(el_count):
            el = self._read_utf16_str()
            self.elements.append(el)

        attr_count = self._read_int()
        for _ in range(attr_count):
            attr = self._read_utf16_str()
            self.attributes.append(attr)

        return self._parse_node()

class XMBWriter:
    def __init__(self, node, fmt=XMBFormat.AOE3):
        self.root = node
        self.format = XMBFormat(fmt) # Allows passing XMBFormat or int
        self.elements = collections.OrderedDict()
        self.attributes = collections.OrderedDict()
        self.stream = None

    def _preprocess(self, node):
        """
        Preprocess the node tree for element and attribute names
        """
        namel = node.name.lower()
        if namel not in self.elements:
            self.elements[namel] = len(self.elements)

        for attr in node.attrs.keys():
            attrl = attr.lower()
            if attrl not in self.attributes:
                self.attributes[attrl] = len(self.attributes)

        for child in node.children:
            self._preprocess(child)


    def write(self, stream):
        """
        Write to a stream
        """
        self.stream = stream
        self._preprocess(self.root)

        self.stream.write(XMB_MAGIC)

        el_data = b''.join(to_ustr(el) for el in self.elements.keys())
        at_data = b''.join(to_ustr(at) for at in self.attributes.keys())

        # Length field does not include the "X1" magic, or the length field
        # itself
        length = self.root.xmb_length()
        length += XMB_ROOT_META_SZ
        length += len(el_data)
        length += len(at_data)

        self.stream.write(struct.pack('<I', length))

        hdr_data = struct.pack(XMB_ROOT_HDR,
                XMB_ROOT_MAGIC,
                XMB_UNK_VAL, 
                self.format)

        self.stream.write(hdr_data)

        self.stream.write(struct.pack('<I', len(self.elements)))
        self.stream.write(el_data)
        self.stream.write(struct.pack('<I', len(self.attributes)))
        self.stream.write(at_data)

        self.root.write_xmb(stream, self)

MIN_BYTES_TO_READ = max(len(L33T_MAGIC), len(XMB_MAGIC), len(ALZ4_MAGIC))
def detect_file_format(filename, read_whole_file=False):
    "Detect format of file"
    with open (filename, 'rb') as f:
        if not read_whole_file:
            data = f.read(MIN_BYTES_TO_READ)
        else:
            data = f.read()

    return detect_format(data)

def detect_format(data):
    """
    Detect format of data
    """
    if data[0:len(L33T_MAGIC)] == L33T_MAGIC:
        return FileType.L33T
    if data[0:len(XMB_MAGIC)] == XMB_MAGIC:
        return FileType.RAW_XMB
    if data[0:len(ALZ4_MAGIC)] == ALZ4_MAGIC:
        return FileType.ALZ4

    # Not really ideal to have to parse the XML to determine if it is an XML
    try:
        etree.fromstring(data)
    except etree.ParseError:
        return FileType.UNKNOWN
    else:
        return FileType.XML

AUTO_FORMAT_MAP = {
    FileType.ALZ4: FileType.XML,
    FileType.L33T: FileType.XML,
    FileType.RAW_XMB: FileType.XML,
}
FORMAT_NAME_MAP= {
    'raw_xmb': FileType.RAW_XMB,
    'xml': FileType.XML,
}

class UnknownDataFormatException(XMBConversionException):
    def __init__(self):
        super().__init__('Data format not recognized')

class UnknownOutputFormatException(XMBConversionException):
    def __init__(self, format_name):
        self.format_name = format_name
        super().__init__('Data format name not recognized: "{}"'.format(format_name))

def choose_output_format(input_format, format_option_str, game):
    """Decide output format based on user input."""
    if format_option_str == 'auto':
        if input_format == FileType.XML:
            if game == 'de':
                return FileType.ALZ4
            else:
                return FileType.L33T

        return AUTO_FORMAT_MAP[input_format]
    else:
        try:
            if format_option_str.strip().lower() == 'xmb':
                if game == 'de':
                    return FileType.ALZ4
                else:
                    return FileType.L33T

            return FORMAT_NAME_MAP[format_option_str.strip().lower()]
        except KeyError:
            raise UnknownOutputFormatException(format_option_str)

def convert_data(input_data, out_format=None, game='de'):
    """
    Convert data to another format

    raises FileFormatNotRecognizedException
    """
    file_type = detect_format(input_data)
    output_format = choose_output_format(file_type, out_format, game)

    if file_type == FileType.L33T:
        input_data = decompress_l33t(input_data)
    elif file_type == FileType.ALZ4:
        input_data = decompress_alz4(input_data)

    if file_type in (FileType.RAW_XMB, FileType.L33T, FileType.ALZ4):
        parser = XMBParser()
        root = parser.parse(io.BytesIO(input_data))
    elif file_type == FileType.XML:
        root = XMBNode.from_xml(input_data)
    elif file_type == FileType.UNKNOWN:
        raise UnknownDataFormatException()

    if output_format in (FileType.RAW_XMB, FileType.L33T, FileType.ALZ4):
        out_stream = io.BytesIO()
        writer = XMBWriter(root)
        writer.write(out_stream)
        out_data = out_stream.getvalue()

        if output_format == FileType.L33T:
            out_data = compress_l33t(out_data)
        elif output_format == FileType.ALZ4:
            out_data = compress_alz4(out_data)

    elif output_format == FileType.XML:
        out_data = bytes(root)

    return out_data

def convert_file(filename, output, format):
    with open(filename, 'rb') as in_file:
        data = in_file.read()

    out_data = convert_data(data, format, 'legacy')
    with open(output, 'wb') if output is not None else sys.stdout as out_file:
        out_file.write(out_data)

def gen_input_output_files(xml_to_xmb):
    "Generate all input and output file names depending on the direction of conversion."
    def is_input_file(filename):
        file_type = detect_file_format(filename, read_whole_file=xml_to_xmb) # XML detected by parsing file
        if xml_to_xmb:
            return file_type == FileType.XML
        else:
            return file_type in [FileType.ALZ4, FileType.RAW_XMB]

    def get_output_filename(workdir, filename):
        base_filename = os.path.splitext(filename)[0]
        print(xml_to_xmb, filename, base_filename, base_filename[-4:].lower())
        if xml_to_xmb:
            return os.path.join(workdir, base_filename + '.xml.XMB')
        elif '.xml' != base_filename[-4:].lower():
            return os.path.join(workdir, base_filename + '.xml')
        else:
            return os.path.join(workdir, base_filename)

    for workdir, folder, files in os.walk('.'):
        for filename in files:
            in_filename = os.path.join(workdir, filename)
            out_filename = get_output_filename(workdir, filename)
            if is_input_file(in_filename):
                yield in_filename, out_filename

def parse_args():
    """Parse commandline arguments."""
    parser = argparse.ArgumentParser(description='Convert between AoE3 file types.')
    parser.add_argument('file', help='Input file. Choose action "all" to apply to every file in tree')
    parser.add_argument('-o', '--output', default=None, help='Output file; defaults to standard out, ignored if converting all files')
    parser.add_argument('--format', default='auto', help='Output format: xmb|xml|raw_xmb')
    parser.add_argument('--unconvert', default=False, action="store_true", help='Convert all XMLs to XMBs if "all" is chosen')
    #parser.add_argument('--game', default='de', help='Select whether XMBs are formatted for AoE3:DE or AoE3 (2007). Supported values are de|legacy; only affects --format=xmb')
    return parser.parse_args()

def main():
    """Convert a file"""
    args = parse_args()

    if args.file == "all":
        for filename, output in gen_input_output_files(args.unconvert):
            try:
                convert_file(filename, output, args.format)
            except XMBException as e:
                print(f'Failed to handle file "{filename}" from {repr(e)}, continuing...')
    else:
        convert_file(args.file, args.output, args.format)

if __name__ == '__main__':
    main()
