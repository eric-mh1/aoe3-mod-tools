"""
Shared exception classes
"""

class AoEFileFormatException(Exception):
    pass

class ALZ4InvalidStream(AoEFileFormatException):
    """
    Raised during ALZ4 stream decompression
    """
    pass

