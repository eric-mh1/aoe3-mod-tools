#!/usr/bin/python3
"""
Functions for the ALZ4 compressed file format
"""
import struct

import lz4.block

from .exception import AoEFileFormatException, ALZ4InvalidStream

ALZ4_MAGIC = b'alz4'

def compress_alz4(data):
    """
    Compress an alz4 data stream
    """
    alz4_data = lz4.block.compress(data, store_size=False)
    alz4_data = struct.pack('<4sIII', ALZ4_MAGIC, len(data), len(alz4_data), 1) + alz4_data
    return alz4_data

def decompress_alz4(data):
    """
    Decompress an alz4 data stream

    alz4 format is a header + lz4 stream
    """
    magic, size, compressed_size = struct.unpack_from('<4sII', data)

    if magic != ALZ4_MAGIC:
        raise ALZ4InvalidStream('Invalid ALZ4 stream: bad magic "{}"'.format(magic))

    output = lz4.block.decompress(data[16:], uncompressed_size=size)
    if len(output) != size:
        raise ALZ4InvalidStream('Invalid ALZ4 stream: incorrect size "{}"'.format(size))

    return output
