#!/usr/bin/python3
import argparse
import os
import struct

from .alz4 import decompress_alz4
from .exception import AoEFileFormatException

class BARException(AoEFileFormatException):
    pass

HEADER_MAGIC = b'ESPN'
HEADER_SIZE = 0x124
BAD_VAL_FMT = 'Bad value at ADDR=0x{:08X}: should be 0x{:08X}, not 0x{:08X}'
BLOCK_SIZE = 4096

BAR_VERSION_AOE3 = 2
BAR_VERSION_AOE3DE = 5

ENCODE_TYPE_NONE = 0
ENCODE_TYPE_ALZ4 = 1
ENCODE_TYPE_SND = 2 # Obfuscated sound file encoding

def read_string(file_handle):
    length = struct.unpack('I', file_handle.read(4))[0]
    data = file_handle.read(length * 2)
    return data.decode('utf-16-le')

def encode_string(s):
    bytestr = s.encode('utf-16-le')
    length = len(bytestr) // 2
    return struct.pack('I', length) + bytestr

def addq(a, b):
    return (a + b) & (2**64-1)

def multq(a, b):
    return (a * b) & (2**64-1)

def rotlq(val, n):
    """
    Left rotate a quadword
    """
    return ((val << n) | (val >> (64 - n))) & (2**64-1)

def decode_snd(stream):
    a = 2564355413007450943
    b = 4159887087525648499
    c = 3096547199993908069

    for block in stream:
        block_out = b''
        while len(block) >= 8:
            qw = struct.unpack_from('<Q', block)[0]
            c = rotlq(multq(b, addq(c, a)), 32)
            block_out += struct.pack('<Q', qw ^ c)
            block = block[8:]

        yield block_out

class ArchiveEntrySource:
    def __init__(self, bar_file_path, file_offset, length):
        self.bar_file_path = os.path.abspath(bar_file_path)
        self.file_offset = file_offset
        self.length = length

    def get_data_stream(self):
        with open(self.bar_file_path, 'rb') as source:
            source.seek(self.file_offset)
            length = self.length
            while length > 0:
                data = source.read(min(BLOCK_SIZE, length))
                length -= len(data)
                yield data


class FileSystemSource:
    def __init__(self, file_path):
        self.file_path = os.path.abspath(file_path)

    def get_data_stream(self):
        with open(self.file_path, 'rb') as source:
            while 1:
                data = source.read(BLOCK_SIZE)
                if len(data) == 0:
                    break
                yield data


class BARFileEntry:
    @classmethod
    def from_bar(cls, file_path, file_handle, version=BAR_VERSION_AOE3):
        if version == BAR_VERSION_AOE3:
            fmt = 'I'
        elif version == BAR_VERSION_AOE3DE:
            fmt = 'Q'
        offset = struct.unpack(fmt, file_handle.read(struct.calcsize(fmt)))[0]

        if version == BAR_VERSION_AOE3DE:
            fmt = 'I'
            file_handle.read(struct.calcsize(fmt))

        compressed_length, uncompressed_length = struct.unpack('II', file_handle.read(8))

        date_bytes = file_handle.read(16)  # Throw these away for now
        file_name = read_string(file_handle)

        if version == BAR_VERSION_AOE3DE:
            compression_type = struct.unpack('I', file_handle.read(4))[0]
        else:
            compression_type = 0

        source = ArchiveEntrySource(file_path, offset, compressed_length)
        return cls(file_name, compressed_length, source, compression_type)

    @classmethod
    def from_filesystem(cls, name, path):
        length = os.stat(path).st_size
        source = FileSystemSource(path)
        return cls(name, length, source, False)

    def __init__(self, file_name, length, source, compression_type):
        self.file_name = file_name
        self.length = length
        self.source = source
        self.compression_type = compression_type

    def entry(self, offset):
        return struct.pack('III', offset, self.length, self.length) + (b'\xCC' * 16) + encode_string(self.file_name)

    def get_data_stream(self):
        return self.source.get_data_stream()

    def extract(self, destination, flatten=False, decompress=True):
        norm_file_name = self.file_name.replace('\\', '/')

        if flatten:
            target_path = os.path.join(destination, os.path.basename(norm_file_name))
        else:
            target_path = os.path.join(destination, norm_file_name)
            try:
                os.makedirs(os.path.dirname(target_path))
            except FileExistsError:
                pass

        print('Extracting "{}"'.format(target_path))
        with open(target_path, 'wb') as f:
            if decompress and self.compression_type == ENCODE_TYPE_ALZ4:
                data = b''.join(self.get_data_stream())
                f.write(decompress_alz4(data))
            elif decompress and self.compression_type == ENCODE_TYPE_SND:
                for block in decode_snd(self.get_data_stream()):
                    f.write(block)
            else:
                for data in self.get_data_stream():
                    f.write(data)

        return target_path

class ArchiveSource:
    def __init__(self, bar_file_path):
        self.file_path = bar_file_path

        self.file_count = None
        self.filetable_offset = None

    def parse_header(self):
        with open(self.file_path, 'rb') as file_handle:
            if file_handle.read(4) != HEADER_MAGIC:
                raise BARException('File is not a BAR file: bad magic')

            self.version, magic2 = struct.unpack('II', file_handle.read(8))
            if magic2 != 0x44332211:
                raise BARException('File is not a BAR file: bad magic 2')

            if self.version not in (BAR_VERSION_AOE3, BAR_VERSION_AOE3DE):
                raise BARException('BAR version {} is not supported'.format(self.version))

            file_handle.seek(0x118)

            if self.version == BAR_VERSION_AOE3:
                fmt = 'II'
            elif self.version == BAR_VERSION_AOE3DE:
                fmt = 'QQ'
            self.file_count, self.filetable_offset = struct.unpack(fmt, file_handle.read(struct.calcsize(fmt)))

    def parse_files(self):
        with open(self.file_path, 'rb') as file_handle:
            file_handle.seek(self.filetable_offset, os.SEEK_SET)
            self.root_name = read_string(file_handle)

            root_file_count = struct.unpack('I', file_handle.read(4))[0]
            if root_file_count != self.file_count:
                raise BARException('File count does not match root file count (main={}, root={})'.format(
                    self.file_count,
                    root_file_count
                    ))

            for file_index in range(root_file_count):
                yield BARFileEntry.from_bar(self.file_path, file_handle, version=self.version)

class BARReader:
    @classmethod
    def from_bar(cls, file_path):
        source = ArchiveSource(file_path)
        source.parse_header()
        files = []
        for entry in source.parse_files():
            files.append(entry)

        return cls(source.root_name, files, source=source)

    @classmethod
    def from_folder(cls, path):
        root_path = os.path.abspath(path)
        self = cls(os.path.basename(root_path) + '\\')

        for root, dirs, files in os.walk(path):
            for f in files:
                file_path = os.path.abspath(os.path.join(root, f))
                file_name = os.path.relpath(file_path, root_path)

                self.files.append(BARFileEntry.from_filesystem(file_name, file_path))
                
        return self

    def __init__(self, root_name, files=None, source=None):
        self.root_name = root_name
        self.source = source
        self.files = []
        if files is not None:
            self.files.extend(files)

    def extract(self, destination, **kwargs):
        for f in self.files:
            f.extract(destination, **kwargs)

    def save(self, destination):
        with open(destination, 'wb+') as sink:
            # Write header
            sink.write(HEADER_MAGIC)
            sink.write(struct.pack('II', 2, 0x44332211))
            sink.write(b'\x00' * 0x10C)
            sink.write(struct.pack('III', len(self.files), 0, 0))

            files = []

            addr = HEADER_SIZE
            for f in self.files:
                files.append((addr, f))
                for data in f.get_data_stream():
                    sink.write(data)
                    addr += len(data)

            # Save table offet
            file_table_offset = addr

            # Write root name
            sink.write(encode_string(self.root_name))
            sink.write(struct.pack('I', len(self.files)))

            # Write file table
            for offset, f in files:
                sink.write(f.entry(offset))

            # Patch table offset into file size
            sink.seek(HEADER_SIZE - 8)
            sink.write(struct.pack('I', file_table_offset))


def list_bar(args):
    reader = BARReader.from_bar(args.bar_file)
    for f in reader.files:
        print(f.file_name)

def build(args):
    writer = BARReader.from_folder(args.directory)
    writer.save(args.target_file)

def extract(args):
    reader = BARReader.from_bar(args.target_file)

    if args.item:
        for f in reader.files:
            if (f.file_name == args.item):
                f.extract(args.directory, flatten=args.flatten)
                return

    reader.extract(args.directory, flatten=args.flatten)

def main():
    parser = argparse.ArgumentParser(description='Process AoE3 .BAR files.')

    subparsers = parser.add_subparsers()
    
    build_cmd = subparsers.add_parser('build', help='Create a BAR archive')
    build_cmd.add_argument('directory')
    build_cmd.add_argument('target_file')
    build_cmd.set_defaults(func=build)

    list_cmd = subparsers.add_parser('list', help='List files in a BAR archive')
    list_cmd.add_argument('bar_file')
    list_cmd.set_defaults(func=list_bar)
    
    extract_cmd = subparsers.add_parser('extract', help='Extract files from a BAR archive')
    extract_cmd.add_argument('target_file')
    extract_cmd.add_argument('directory')
    extract_cmd.add_argument('--item')
    extract_cmd.add_argument('--flatten', action='store_true')
    extract_cmd.set_defaults(func=extract)
    
    args = parser.parse_args()
    if not hasattr(args, 'func'):
        parser.print_help()
        return

    try:
        args.func(args)
    except BrokenPipeError:
        # Occurs e.g., if a user pipes output to `head` during a `list` action.
        pass

if __name__ == '__main__':
    main()
