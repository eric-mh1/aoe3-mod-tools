import argparse
import os
import lxml.etree as ET

MSG_INVALID_DBID = '{} has an invalid DBID: "{}", DBID="{}"'
MSG_MISSING_DBID = '{} has no DBID field: "{}"'
MSG_DUPLICATE_DBID = '{} using DBID already in use: "{}", DBID="{}"'
MSG_CORRECTING_TAG_CASE = 'XML tag had incorrect case: {}="{}", tag="{}"'

MSG_OUTPUT_EXISTS = 'Output file "{}" already exists.\n Please delete or specify another name with --output'
MSG_INPUT_MISSING = 'Input file "{}" does not exist.\n Please specify another name with --input'
MSG_CANNOT_OPEN_INPUT = 'Cannot open input file "{}" for reading.'
MSG_CANNOT_OPEN_OUTPUT = 'Cannot open output file "{}" for writing.'
MSG_BAD_ROOT = 'Unrecognized XML file with root element "{}"'
MSG_COMPLETE = 'Done. Repaired {} errors.'

SHOW_ALWAYS = 1000
SHOW_VERBOSE = 1
verbosity = SHOW_ALWAYS

def set_verbose(setting):
    global verbosity

    if setting:
        verbosity = SHOW_VERBOSE

def log(verbosity_level, *args, **kwargs):
    if (verbosity_level >= verbosity):
        print(*args, **kwargs)

def xml_find_icase(element, tag):
    for child in element:
        
        if type(child.tag) != str:
            continue

        if child.tag.lower() == tag.lower():
            return child

def xml_find_or_create(element, tag):
    child_element = element.find(tag)
    if child_element is None:
        child_element = ET.SubElement(element, tag)

    return child_element


def generate_dbid(used_dbids, start=1):
    while start in used_dbids:
        start += 1
    return start

    
def dbid_fix(args):
    set_verbose(args.verbose)
    
    if not os.path.exists(args.input):
        print(MSG_INPUT_MISSING.format(args.input))
        return

    if os.path.exists(args.output):
        print(MSG_OUTPUT_EXISTS.format(args.output))
        return
        
    try:
        item_tree = ET.parse(args.input)
    except OSError:
        print(MSG_CANNOT_OPEN_INPUT.format(args.input))
        return     

    try:
        with open(args.output, 'w') as f:
            pass
    except OSError:
        print(MSG_CANNOT_OPEN_OUTPUT.format(args.output))
        return
        
    root = item_tree.getroot()
    if root.tag.lower() == 'Proto'.lower():
        item_tag = 'Unit'

    elif root.tag.lower() == 'TechTree'.lower():
        item_tag = 'Tech'

    else:
        print(MSG_BAD_ROOT.format(root.tag))
        return

    items_to_fix = []
    dbids = []
    error_count = 0

    # Gather list of used DBIDs and Techs/Units that need to be fixed
    for item in root.findall(item_tag):
        name = item.attrib['name']
        child_element = xml_find_icase(item, 'DBID')
        if child_element == None:
            log(SHOW_VERBOSE, MSG_MISSING_DBID.format(item_tag, name))
            items_to_fix.append(item)
            error_count += 1
            continue

        # Fix tag case if it is incorrect
        if child_element.tag != 'DBID':
            log(SHOW_VERBOSE, MSG_CORRECTING_TAG_CASE.format(item_tag, name, child_element.tag))
            child_element.tag = 'DBID'
            error_count += 1

        dbid = child_element.text

        try:
            dbid = int(dbid)
        except (ValueError, TypeError):
            log(SHOW_VERBOSE, MSG_INVALID_DBID.format(item_tag, name, dbid))
            items_to_fix.append(item)
            error_count += 1
            continue
            
        if dbid in dbids:
            log(SHOW_VERBOSE, MSG_DUPLICATE_DBID.format(item_tag, name, dbid))
            items_to_fix.append(item)
            error_count += 1
            continue
            
        dbids.append(dbid)
            
    # Loop through queued Techs/Units to fix
    new_dbid = 1
    for item in items_to_fix:
        new_dbid = generate_dbid(dbids, new_dbid)
        child_element = xml_find_or_create(item, 'DBID').text = str(new_dbid)
        new_dbid += 1
 
    tempfile = os.path.basename(args.output) + '.tmp'
    item_tree.write(tempfile, encoding='utf-8', xml_declaration=True)

    # Hotpatch quotes (careful if applying this to something that might have quotes e.g., stringtable!)

    BLOCK_SIZE = 1024

    with open(tempfile, 'r') as tmpf, open(args.output, 'w') as f:
        while True:
            text = tmpf.read(BLOCK_SIZE)
            text.replace('"', '\'')
            f.write(text)

            if len(text) < BLOCK_SIZE:
                break

    os.remove(tempfile)
    log(SHOW_ALWAYS, MSG_COMPLETE.format(error_count))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Resolve DBID conflicts.')
    parser.add_argument('-v', '--verbose', help='display more messages', action='store_true')
    parser.add_argument('--output', default='tree_out.xml', help='output filename')
    parser.add_argument('input', help='input filename')
    
    args = parser.parse_args()

    try:
        dbid_fix(args)
    except:
        print('The following error occurred:')
        print('====')
        import traceback
        traceback.print_exc()
        input('Press enter to close...')
